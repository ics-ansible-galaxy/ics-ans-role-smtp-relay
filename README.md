# ics-ans-role-smtp-relay

Ansible role to install smtp-relay.

## Role Variables

```yaml
This role install postfix in a SMTP relay mode.
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-smtp-relay
```

## License

BSD 2-clause
