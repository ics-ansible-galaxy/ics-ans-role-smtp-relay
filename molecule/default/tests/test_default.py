import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_postfi(host):
    # TODO: implement at least a test
    service = host.service("postfix")
    assert service.is_running
    assert service.is_enabled
